class AuthorsEvents {
  AuthorsEvents();
}

class GetAllAuthorsEvent extends AuthorsEvents {
  GetAllAuthorsEvent();
}

class GetMoreAuthorsEvent extends AuthorsEvents {
  int page;
  int limit;
  GetMoreAuthorsEvent({this.page, this.limit});
}

