import 'package:inetum/models/authorModel.dart';

class AuthorsStates {
  AuthorsStates();
}

class AuthorsInitialState extends AuthorsStates {
  AuthorsInitialState();
}

class LoadingAuthorsState extends AuthorsStates {
  LoadingAuthorsState();
}

class AuthoresLoadedSuccessState extends AuthorsStates {
  List<AuthorModel> authorsList;
  AuthoresLoadedSuccessState({this.authorsList});
}

class NoAuthorsFoundState extends AuthorsStates {
  NoAuthorsFoundState();
}

class FailedToGetAuthorsState extends AuthorsStates {
  FailedToGetAuthorsState();
}

class LoseConnectionState extends AuthorsStates {
  LoseConnectionState();
}

