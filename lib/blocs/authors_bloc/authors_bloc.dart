
import 'package:bloc/bloc.dart';
import 'package:inetum/blocs/authors_bloc/events/authors_events.dart';
import 'package:inetum/blocs/authors_bloc/states/authors_states.dart';
import 'package:inetum/exceptions/lose_connection_exception.dart';
import 'package:inetum/repositories/authors_repository.dart';




class AuthorsBloc extends Bloc<AuthorsEvents, AuthorsStates> {
  final AuthorsRepository authorsRepository = AuthorsRepository();

  AuthorsBloc() : super(AuthorsInitialState());

  @override
  Stream<AuthorsStates> mapEventToState(AuthorsEvents event) async* {
    if (event is GetAllAuthorsEvent) {
      yield LoadingAuthorsState();
      try {
        final result = await authorsRepository.getAllAuthors();
        if (result.length == 0) {
          yield NoAuthorsFoundState();
        } else {
          yield AuthoresLoadedSuccessState(authorsList: result);
        }
      } on LoseConnectionException catch (exception) {
        yield LoseConnectionState();
      } catch (exception2) {
        yield FailedToGetAuthorsState();
      }
    }
}}