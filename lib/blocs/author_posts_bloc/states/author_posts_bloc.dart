import 'package:inetum/models/postsAuthorModel.dart';

class AuthorPostsStates {
  AuthorPostsStates();
}

class AuthorsInitialState extends AuthorPostsStates {
  AuthorsInitialState();
}

class LoadingAuthorsState extends AuthorPostsStates {
  LoadingAuthorsState();
}

class AuthorePostsLoadedSuccessState extends AuthorPostsStates {
  List<PostsAuthorModel> postsAuthorList;
  AuthorePostsLoadedSuccessState({this.postsAuthorList});
}

class NoAuthorPostsFoundState extends AuthorPostsStates {
  NoAuthorPostsFoundState();
}

class FailedToGetAuthorPostsState extends AuthorPostsStates {
  FailedToGetAuthorPostsState();
}

class LoseConnectionState extends AuthorPostsStates {
  LoseConnectionState();
}

