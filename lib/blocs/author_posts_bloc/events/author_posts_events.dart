class AuthorPostsEvents {
  AuthorPostsEvents();
}

class GetPostsAuthorByIdEvent extends AuthorPostsEvents {
  int authorId;
  GetPostsAuthorByIdEvent({this.authorId});
}

class GetMoreAuthorPostsEvent extends AuthorPostsEvents {
  int page;
  int limit;
  GetMoreAuthorPostsEvent({this.page, this.limit});
}

