
import 'package:bloc/bloc.dart';
import 'package:inetum/blocs/author_posts_bloc/events/author_posts_events.dart';
import 'package:inetum/blocs/author_posts_bloc/states/author_posts_bloc.dart';
import 'package:inetum/exceptions/lose_connection_exception.dart';
import 'package:inetum/repositories/author_posts_repository.dart';




class AuthorPostsBloc extends Bloc<AuthorPostsEvents, AuthorPostsStates> {
  final AuthorPostsRepository authorPostsRepository = AuthorPostsRepository();

  AuthorPostsBloc() : super(AuthorsInitialState());

  @override
  Stream<AuthorPostsStates> mapEventToState(AuthorPostsEvents event) async* {
    if (event is GetPostsAuthorByIdEvent) {
      yield LoadingAuthorsState();
      try {
        final result = await authorPostsRepository.getAllAuthorPosts(autorId: event.authorId);
        if (result.length == 0) {
          yield NoAuthorPostsFoundState();
        } else {
          yield AuthorePostsLoadedSuccessState(postsAuthorList: result);
        }
      } on LoseConnectionException catch (exception) {
        yield LoseConnectionState();
      } catch (exception2) {
        yield FailedToGetAuthorPostsState();
      }
    }
}}