import 'dart:convert';

import 'package:inetum/data/authors_api_client.dart';
import 'package:inetum/models/authorModel.dart';

class AuthorsRepository {
  AuthorsApiClient authorsApiClient = AuthorsApiClient();

  Future getAllAuthors() async {
    try {
      final authors = await authorsApiClient.getAuthorData();
      List<dynamic> mapObject = jsonDecode(authors);
      List<AuthorModel> allAuthorsData =
          mapObject.map((data) => AuthorModel.fromJson(data)).toList();
      return allAuthorsData;
    } catch (exception) {
      print(exception);
      throw exception;
    }
  }
}
