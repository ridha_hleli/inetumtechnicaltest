import 'dart:convert';

import 'package:inetum/data/authors_api_client.dart';
import 'package:inetum/models/postsAuthorModel.dart';

class AuthorPostsRepository {
  AuthorsApiClient authorsApiClient = AuthorsApiClient();

  Future getAllAuthorPosts({int autorId}) async {
    try {
      final authorPosts = await authorsApiClient.getAuthorPosts(autorId: autorId);
      List mapObject = jsonDecode(authorPosts);
      print("mapObject $mapObject");
      List<PostsAuthorModel> allAuthorsData =
          mapObject.map((data) => PostsAuthorModel.fromJson(data)).toList();
      return allAuthorsData;
    } catch (exception) {
      print(exception);
      throw exception;
    }
  }
}
