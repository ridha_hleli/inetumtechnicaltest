class LoseConnectionException implements Exception {
  final String message;
  LoseConnectionException({this.message});
}
