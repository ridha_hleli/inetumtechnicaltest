import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'package:inetum/exceptions/lose_connection_exception.dart';

class AuthorsApiClient{


  Future<void> checkConnection() async {
    if ((await Connectivity().checkConnectivity()) == ConnectivityResult.none)
      throw LoseConnectionException();
  }


  Future getAuthorData() async {
    await checkConnection();
    var response =
        await http.get(Uri.https('sym-json-server.herokuapp.com', 'authors'));
    return response.body;
  }

    Future getAuthorPosts({int autorId}) async {
    await checkConnection();
    var response = await  http.get(Uri.parse('https://sym-json-server.herokuapp.com/posts?authorId=$autorId'));
    return response.body;
  }
}