import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:inetum/blocs/author_posts_bloc/author_posts_bloc.dart';
import 'package:inetum/blocs/author_posts_bloc/events/author_posts_events.dart';
import 'package:inetum/blocs/author_posts_bloc/states/author_posts_bloc.dart';
import 'package:inetum/models/authorModel.dart';
import 'package:inetum/screens/failedConnectionScreen/failedConnectionScreen.dart';
import 'package:lottie/lottie.dart';
import 'package:url_launcher/url_launcher.dart';

class AuthorDetailsScreen extends StatefulWidget {
  final AuthorModel authorData;
  AuthorDetailsScreen({this.authorData});
  @override
  _AuthorDetailsScreenState createState() => _AuthorDetailsScreenState();
}

class _AuthorDetailsScreenState extends State<AuthorDetailsScreen> {
  AuthorPostsBloc authorPostsBloc;

  @override
  void initState() {
    super.initState();
    authorPostsBloc = AuthorPostsBloc();
    authorPostsBloc.add(
      GetPostsAuthorByIdEvent(authorId: widget.authorData.id),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text(
          "Details",
          style: TextStyle(color: Colors.black54),
        )),
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black54,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.location_on,
                color: Colors.red,
              ),
              onPressed: () async {
                print("lat ${widget.authorData.address.latitude}");
                print("longitude ${widget.authorData.address.longitude}");
                final String googleMapsUrl =
                    "https://www.google.com/maps/search/?api=1&query=${double.parse(widget.authorData.address.latitude)}%2C${double.parse(widget.authorData.address.longitude)}";

                if (await canLaunch(googleMapsUrl)) {
                  await launch(googleMapsUrl);
                }
              })
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 15, bottom: 10),
            child: Row(
              children: [
                Hero(
                  tag: widget.authorData.id,
                  child: Container(
                    height: 100.0,
                    width: 100.0,
                    child: CachedNetworkImage(
                      imageUrl: widget.authorData.avatarUrl,
                      placeholder: (context, url) =>
                          Lottie.asset("assets/images/load_image.json"),
                      errorWidget: (context, url, error) =>
                          Lottie.asset("assets/images/notFound.json"),
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Row(
                          children: [
                            Icon(Icons.person_outline),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              widget.authorData.name,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Row(
                          children: [
                            Icon(Icons.person_outline),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              widget.authorData.userName,
                              style: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Row(
                          children: [
                            Icon(Icons.mail_outline),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              widget.authorData.email,
                              style: TextStyle(
                                  fontWeight: FontWeight.w300, fontSize: 12),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: BlocBuilder(
              bloc: authorPostsBloc,
              builder: (context, state) {
                if (state is LoadingAuthorsState) {
                  return Container(
                    child: Center(
                      child: SpinKitRipple(
                        color: Colors.blue,
                      ),
                    ),
                  );
                } else if (state is AuthorePostsLoadedSuccessState) {
                  return ListView.builder(
                    itemCount: state.postsAuthorList.length,
                    itemBuilder: (context, index) {
                      return Center(
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Card(
                            clipBehavior: Clip.antiAlias,
                            child: InkWell(
                              onTap: () {},
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Stack(
                                    alignment: Alignment.bottomLeft,
                                    children: [
                                      CachedNetworkImage(
                                        imageUrl: state
                                            .postsAuthorList[index].imageUrl,
                                        placeholder: (context, url) =>
                                            Lottie.asset(
                                                "assets/images/load_image.json"),
                                        errorWidget: (context, url, error) =>
                                            Lottie.asset(
                                                "assets/images/notFound.json"),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 16.0,
                                        top: 16.0,
                                        right: 16.0,
                                        bottom: 10.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          state.postsAuthorList[index].title,
                                          style: TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.w500),
                                          textAlign: TextAlign.start,
                                        ),
                                        SizedBox(height: 10),
                                        Text(
                                          state.postsAuthorList[index].body,
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              fontWeight: FontWeight.w300),
                                          textAlign: TextAlign.justify,
                                        ),
                                        SizedBox(height: 5.0),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              state.postsAuthorList[index].date,
                                              style: TextStyle(
                                                  color: Colors.red[200],
                                                  fontSize: 10.0,
                                                  fontWeight: FontWeight.w900),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  );
                } else if (state is LoseConnectionState) {
                  return FailedConnectionScreen(
                    func: () {
                      authorPostsBloc.add(GetPostsAuthorByIdEvent(
                          authorId: widget.authorData.id));
                    },
                  );
                } else {
                  return Container();
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
