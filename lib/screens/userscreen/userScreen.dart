import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:inetum/blocs/authors_bloc/authors_bloc.dart';
import 'package:inetum/blocs/authors_bloc/events/authors_events.dart';
import 'package:inetum/blocs/authors_bloc/states/authors_states.dart';
import 'package:inetum/screens/authorDetailsScreen/authorDetailScreen.dart';
import 'package:inetum/screens/failedConnectionScreen/failedConnectionScreen.dart';
import 'package:lottie/lottie.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  AuthorsBloc authorsBloc;

  @override
  void initState() {
    super.initState();
    authorsBloc = AuthorsBloc();
    authorsBloc.add(GetAllAuthorsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Center(
          child: Text(
            "Authors",
            style: TextStyle(
              color: Colors.black54,
            ),
          ),
        ),
      ),
      body: BlocBuilder(
        bloc: authorsBloc,
        builder: (context, state) {
          if (state is LoadingAuthorsState) {
            return Container(
              child: Center(
                child: SpinKitRipple(
                  color: Colors.blue,
                ),
              ),
            );
          } else if (state is AuthoresLoadedSuccessState) {
            return ListView.builder(
              itemCount: state.authorsList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AuthorDetailsScreen(
                          authorData: state.authorsList[index],
                        ),
                      ),
                    );
                  },
                  leading: Hero(
                    tag: state.authorsList[index].id,
                    child: CachedNetworkImage(
                      imageUrl: state.authorsList[index].avatarUrl,
                      placeholder: (context, url) =>
                          Lottie.asset("assets/images/load_image.json"),
                      errorWidget: (context, url, error) =>
                          Lottie.asset("assets/images/notFound.json"),
                    ),
                  ),
                  title: Text(state.authorsList[index].name),
                  subtitle: Text(state.authorsList[index].userName),
                  trailing: Icon(Icons.arrow_forward_ios),
                );
              },
            );
          } else if (state is LoseConnectionState) {
            return FailedConnectionScreen(
              func: () {
                authorsBloc.add(GetAllAuthorsEvent());
              },
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
