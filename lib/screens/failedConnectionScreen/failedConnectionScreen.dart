import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class FailedConnectionScreen extends StatelessWidget {
 final Function func;
  FailedConnectionScreen({this.func});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 300,
              height: 300,
              child: Lottie.asset('assets/images/failed.json'),
            ),
            SizedBox(height: 30),
            Text(
              "Connection Failed",
              style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
                color: Colors.black54,
              ),
            ),
            SizedBox(height: 30),
            Text(
              "Could not connect to the network \nPlease check and try again",
              style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w400,
                color: Colors.black54,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 30,
            ),
            FloatingActionButton.extended(
              onPressed: func,
              label: Text("Retry"),
              backgroundColor: Colors.cyan[700],
            )
          ],
        ),
      ),
    );
  }
}
