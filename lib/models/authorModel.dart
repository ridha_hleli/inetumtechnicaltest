
import 'package:inetum/models/adressModel.dart';

class AuthorModel {
  int id;
  String name;
  String userName;
  String email;
  String avatarUrl;
  AddressModel address;

  AuthorModel(
      {this.id,
      this.name,
      this.userName,
      this.email,
      this.avatarUrl,
      this.address});

  AuthorModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    userName = json['userName'];
    email = json['email'];
    avatarUrl = json['avatarUrl'];
    address =
        json['address'] != null ? new AddressModel.fromJson(json['address']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['userName'] = this.userName;
    data['email'] = this.email;
    data['avatarUrl'] = this.avatarUrl;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    return data;
  }
}